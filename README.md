# Good-Bye Bitbucket

_An open letter to self._

I love Atlassian, and the many great products offered.
I've closed out all my projects hosted by Bitbucket in an effort to free-up time & energy elsewhere.

Perhaps I'll push in the new year.

~emcconville
